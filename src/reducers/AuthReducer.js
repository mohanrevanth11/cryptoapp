//Deal with everything related to authentication

import {
    ON_EMAIL_CHANGE,
    ON_PASSWORD_CHANGE,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER,    
} from '../actions/types';

const INITIAL_STATE = {
    email: '',
    password: '',
    user: null,
    error: '',
    loading: false
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case ON_EMAIL_CHANGE:
            return {...state, email:action.payload};
        case ON_PASSWORD_CHANGE:
            return {...state, password:action.payload};
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.payload, error: '', loading: false, email:'', password: ''};
        case LOGIN_USER_FAIL:
            return {...state, error: 'Authentication Failed.', user: null, password: '', loading: false};
        case LOGIN_USER:
            return {...state, loading: true, error: ''};
        default:
            return state;
    }

};