import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import ListReducer from './ListReducer';

export default combineReducers({
    //Key is the property of the state
    auth: AuthReducer,
    currencyList: ListReducer
});