import {CURRENCY_LIST,CURRENCY_LIST_SUCCESS,CURRENCY_LIST_FAIL} from '../actions/types';

const INITIAL_STATE = {
    data: [],
    error: '',
    loading: false
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case CURRENCY_LIST:
            return {...state, loading: true, error: ''};
        case CURRENCY_LIST_SUCCESS:
            return {...state, data: action.payload, error: '', loading: false};
        case CURRENCY_LIST_FAIL:
            return {...state, error: 'Request failed', data: null, loading: false};        
        default:
            return state;
    }

};