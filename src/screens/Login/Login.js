import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {onEmailChange, onPasswordChange, loginUser} from '../../actions';
import {CardSection, Input, Button, Spinner} from '../../components/common';

class LoginForm extends Component{

    componentWillReceiveProps(newProps){
        if(newProps.user !== null)
        {
            this.props.navigation.navigate('App');
        }
    }

    renderError(){
        if(this.props.error){
            return(
                <View style= {{backgroundColor: 'white'}}>
                    <Text style= {styles.errorTextStyle}>
                        {this.props.error}
                    </Text>
                </View>
            )
        }
    }

    renderButton(){
        const {email, password, loading, loginUser} = this.props;
        if(loading){
           return <Spinner size="large" />; 
        }
        return(
        <Button onPress = {() => loginUser({email,password})}>
            <Text style={styles.textStyle}>Login</Text>            
        </Button>
        );
    }


    render(){
        return(
            <View style ={{backgroundColor: '#FFF',flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <CardSection>
                    <Input 
                        label="Email"
                        placeHolder = "abc@xyz.com"
                        onChangeText = {(text) => this.props.onEmailChange(text)}
                        value={this.props.email}/>
                </CardSection>

                <CardSection>
                    <Input
                        label = "Password"
                        secureTextEntry
                        placeHolder = "password"
                        onChangeText = {(text) => this.props.onPasswordChange(text)}
                        value = {this.props.password}/>                    
                </CardSection>

                {this.renderError()}                

                <CardSection>                                        
                    {this.renderButton()}
                </CardSection>                
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        password: state.auth.password,
        error: state.auth.error,
        loading: state.auth.loading,
        user: state.auth.user,
    }
}

export default connect (mapStateToProps, {
    onEmailChange, onPasswordChange, loginUser
})(LoginForm);

const styles = StyleSheet.create({
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    textStyle: {    
        color: '#FFF',
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        width: 50,
        fontWeight: 'bold'
      },
});