import React, {Component} from 'react';
import {Text,View, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {getCurrencyList} from '../../actions'
import {Spinner} from '../../components/common'

class List extends Component{
    constructor(props){
        super(props)
        //props.getCurrencyList();
    }
    componentWillMount(){
        this.props.getCurrencyList();
    }
    render(){
        const {loading,data} = this.props;
        return(            
            <View style={{flex:1}}>     
            <Text>{data.Response}</Text>
            {/* <TouchableOpacity onPress = {() => this.props.getCurrencyList()}>
                <Text>On click {data.Data}</Text>
            </TouchableOpacity>                            */}
                {/* <FlatList
                    data = {data.Data}
                    renderItem = {(item) => {
                        <Text>asdfasdf</Text>
                    }}
                /> */}
                {loading?<ActivityIndicator/>:null}                                
            </View>
        );
    }
}

const mapStateToProps = state => {
    return{
        data: state.currencyList.data,
        loading: state.currencyList.loading,
        error: state.currencyList.error
    }
};

export default connect(mapStateToProps,{getCurrencyList})(List);