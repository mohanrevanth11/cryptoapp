import React, {Component} from 'react';
import {createStackNavigator,createSwitchNavigator} from 'react-navigation';
import LoginScreen from '../screens/Login/Login';
import ListScreen from '../screens/Crypto/List';

const AppStackNavigator = createStackNavigator(
    {
        List: ListScreen,
    },{
        initialRouteName: 'List'
    }
);

const RootNavigator = createSwitchNavigator(
    {
        Login: LoginScreen,
        App: AppStackNavigator
    },{
        initialRouteName: 'App',
    }
);


class AppNavigator extends Component{ 
    render(){
        return(
            <RootNavigator/>
        );        
    }   
}
export default AppNavigator;