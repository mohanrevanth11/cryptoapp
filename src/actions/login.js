import firebase from 'firebase';
import {
    ON_EMAIL_CHANGE,
    ON_PASSWORD_CHANGE,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER
} from './types';

export const onEmailChange = (text) => {
    return{
        type: ON_EMAIL_CHANGE,
        payload: text 
    }
}

export const onPasswordChange = (text) => {
    return{
        type: ON_PASSWORD_CHANGE,
        payload: text
    }
}

export const loginUser = ({email, password}) => {
    return (dispatch) => {
    dispatch({type: LOGIN_USER});
    firebase.auth().signInWithEmailAndPassword(email,password)
        .then(user => loginUserSuccess(dispatch, user))
        .catch((error) => {
            console.log(error);
            firebase.auth().createUserWithEmailAndPassword(email,password)
                .then(user => loginUserSuccess(dispatch, user))
                .catch(() => loginUserFail(dispatch));
        });
    };
};

const loginUserSuccess = (dispatch, user) => {
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: user
    });
}

const loginUserFail = (dispatch) => {
    dispatch({type: LOGIN_USER_FAIL});
}