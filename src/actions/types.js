export const ON_EMAIL_CHANGE = 'on_email_change';
export const ON_PASSWORD_CHANGE = 'on_password_change';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const CURRENCY_LIST = 'currency_list';
export const CURRENCY_LIST_SUCCESS = 'currency_list_success';
export const CURRENCY_LIST_FAIL = 'currency_list_fail';