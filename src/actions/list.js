import {CURRENCY_LIST, CURRENCY_LIST_SUCCESS, CURRENCY_LIST_FAIL} from './types';

export const getCurrencyList = () => {
    return (dispatch) => {
    dispatch({type: CURRENCY_LIST});
    fetch('https://min-api.cryptocompare.com/data/all/coinlist')
        .then((res) => res.json())        
        .then(resJson => dispatch({type:CURRENCY_LIST_SUCCESS, payload:resJson}))
        .catch((error) => {
            console.log(error);            
        });
    };
};


// fetch('https://facebook.github.io/react-native/movies.json')
//         .then(response => { console.log(response.json())})        
//         .catch(error => {
//             console.log(error);     
//             throw error;       
//         });
